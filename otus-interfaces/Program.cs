﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace otus_interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener());

            var currencyConverter = new ExchangeRatesApiConverter(new ExchangeRateServiceCBR(), new MemoryCache(new MemoryCacheOptions()));

            var transactionRepository = new InMemoryTransactionRepository();
            //var transactionRepository = new FileTransactionRepository("TransactionStorage.txt");
            var transactionParser = new TransactionParser();

            var budgetApp = new BudjetApplication(transactionRepository, transactionParser, currencyConverter);

            Console.OutputEncoding = System.Text.Encoding.UTF8;

            budgetApp.AddTransaction("Трата -400 RUB Продукты Пятерочка");
            budgetApp.AddTransaction("Зачисление 2000 EUR Зарплата");
            budgetApp.AddTransaction("Перевод -500 USD Петрову Долг");

            budgetApp.OutputTransactions();

            budgetApp.OutputBalanceInCurrency("RUB");
            budgetApp.OutputBalanceInCurrency("EUR");
            budgetApp.OutputBalanceInCurrency("USD");

            Console.Read();
        }
    }
}

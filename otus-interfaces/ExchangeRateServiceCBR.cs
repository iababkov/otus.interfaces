﻿using System;
using System.Net.Http;
using System.Xml;
using System.Threading;
using System.Collections.Generic;

namespace otus_interfaces
{
    class ExchangeRateServiceCBR : IExchangeRateService
    {
        private const string soapNamespace = "http://www.w3.org/2003/05/soap-envelope";
        private const string cbrNamespace = "http://web.cbr.ru/";

        private string _responseBody;

        public decimal GetRate(string fromCurrency, string toCurrency)
        {
            Dictionary<string, decimal> curDict = new Dictionary<string, decimal>();

            GetRatesFromService(ref curDict);

            bool destCurrencyIsRUB = toCurrency == "RUB";

            if (!destCurrencyIsRUB &&
                !curDict.ContainsKey(toCurrency))
            {
                throw new Exception($"Курс валюты {toCurrency} не определён.");
            }

            bool baseCurrencyIsRUB = fromCurrency == "RUB";

            if (!baseCurrencyIsRUB &&
                !curDict.ContainsKey(fromCurrency))
            {
                throw new Exception($"Курс валюты {fromCurrency} не определён.");
            }

            return  destCurrencyIsRUB
                    ? (1 / curDict[fromCurrency])
                    : (curDict[toCurrency] / (baseCurrencyIsRUB ? 1 : curDict[fromCurrency]));
        }

        private void GetRatesFromService(ref Dictionary<string, decimal> curDict)
        {
            string dateTimeStr = "";
            XmlDocument xmlDoc = new XmlDocument();

            RequestService("GetLatestDateTime");
            ValidateResponse(10);

            xmlDoc.LoadXml(_responseBody);
            if (xmlDoc != null)
            {
                XmlNodeList xmlNLDateTime = xmlDoc.GetElementsByTagName("GetLatestDateTimeResult");
                if (xmlNLDateTime != null)
                {
                    dateTimeStr = xmlNLDateTime[0].InnerText;
                }
            }

            if (dateTimeStr == "")
            {
                throw new Exception("Дата курса валют не определена.");
            }

            RequestService("GetCursOnDateXML", "On_date", dateTimeStr);
            ValidateResponse(10);

            xmlDoc.LoadXml(_responseBody);
            if (xmlDoc != null)
            {
                XmlNodeList xmlNLRateList = xmlDoc.GetElementsByTagName("ValuteCursOnDate");
                if (xmlNLRateList != null)
                {
                    foreach (XmlNode n in xmlNLRateList)
                    {
                        curDict.TryAdd(n.SelectSingleNode("VchCode").InnerText,
                                        decimal.Parse(n.SelectSingleNode("Vcurs").InnerText) / decimal.Parse(n.SelectSingleNode("Vnom").InnerText));
                    }
                }
            }
        }

        private void ValidateResponse(int timeOutInSec)
        {
            for (byte i = 0; i < timeOutInSec; i++)
            {
                Thread.Sleep(1000);

                if (_responseBody != "")
                {
                    return;
                }
            }

            throw new Exception("Ответ сервиса не получен.");
        }

        private async void RequestService(string operation, string parmName = "", string parmValue = "")
        {
            _responseBody = "";

            XmlDocument xmlDoc = new XmlDocument();

            XmlElement xmlElEnvelope = xmlDoc.CreateElement("soap12", "Envelope", soapNamespace);
            xmlDoc.AppendChild(xmlElEnvelope);

            XmlElement xmlElBody = xmlDoc.CreateElement("soap12", "Body", soapNamespace);
            xmlElEnvelope.AppendChild(xmlElBody);

            XmlElement xmlElOperation = xmlDoc.CreateElement(operation, cbrNamespace);
            xmlElBody.AppendChild(xmlElOperation);

            if (parmName != "")
            {
                XmlElement xmlElParm = xmlDoc.CreateElement(parmName, cbrNamespace);
                xmlElParm.InnerText = parmValue;
                xmlElOperation.AppendChild(xmlElParm);
            }

            HttpContent content = new StringContent(xmlDoc.OuterXml, System.Text.Encoding.UTF8, "application/soap+xml");

            HttpClient httpClient = new HttpClient();

            HttpResponseMessage response = await httpClient.PostAsync("https://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx", content);
            response.EnsureSuccessStatusCode();

            _responseBody = await response.Content.ReadAsStringAsync();
        }
    }
}
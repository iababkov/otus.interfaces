﻿using System.Collections.Generic;
using System.IO;

namespace otus_interfaces
{
    public class FileTransactionRepository : ITransactionRepository
    {
        public string File { get; }

        public FileTransactionRepository(string file)
        {
            File = file;
        }

        public void AddTransaction(ITransaction transaction)
        {
            using (var sw = new StreamWriter(File, true, System.Text.Encoding.UTF8))
            {
                sw.WriteLine($"{transaction.GetType()} {transaction.Date.ToFileTime()} {transaction.Amount.Amount} {transaction.Amount.CurrencyCode}");
            }
        }

        public ITransaction[] GetTransactions()
        {
            var transactionList = new List<ITransaction>();

            using (var sr = new StreamReader(File, System.Text.Encoding.UTF8))
            {
                string[] fileLineSplitted;

                while (!sr.EndOfStream)
                {
                    fileLineSplitted = sr.ReadLine().Split(' ');
                    //switch (fileLineSplitted[0])
                    //{
                    //    case 
                    //}
                }
            }

            return transactionList.ToArray();
        }
    }

    public class LanguageDictionary
    {
        private Dictionary<string, string> _dict = new Dictionary<string, string>();
        private const string _path = "Storage.txt";

        public void Add(string keyWord, string translation)
        {
            if (!_dict.ContainsKey(keyWord))
            {
                _dict.Add(keyWord, translation);

                using (var sw = new StreamWriter(_path, true))
                {
                    sw.WriteLine($"{keyWord}|{translation}");
                }
            }
        }

        public bool Check(string keyWord, string transaltion)
        {
            return _dict[keyWord].ToLower() == transaltion.ToLower();
        }

        public string Transalte(string keyWord)
        {
            if (_dict.ContainsKey(keyWord))
            {
                return _dict[keyWord];
            }
            else
            {
                return null;
            }
        }

        public LanguageDictionary()
        {
            foreach (var l in File.ReadLines(_path))
            {
                var words = l.Split('|');

                if (words.Length == 2)
                    _dict.Add(words[0], words[1]);
            }
        }
    }
}

﻿namespace otus_interfaces
{
    interface IExchangeRateService
    {
        public decimal GetRate(string fromCurrency, string toCurrency);
    }
}

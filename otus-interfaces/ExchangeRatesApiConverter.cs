﻿using Microsoft.Extensions.Caching.Memory;

namespace otus_interfaces
{
    internal class ExchangeRatesApiConverter : ICurrencyConverter
    {
        IExchangeRateService _exchangeRateService;
        private MemoryCache _memoryCache;

        public ExchangeRatesApiConverter(IExchangeRateService exchangeRateService, MemoryCache memoryCache)
        {
            this._exchangeRateService = exchangeRateService;
            this._memoryCache = memoryCache;
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            return new CurrencyAmount(  currencyCode,
                                        amount.Amount /
                                        _memoryCache.GetOrCreate(   (amount.CurrencyCode, currencyCode),
                                                                    entry => _exchangeRateService.GetRate(amount.CurrencyCode, currencyCode)));
        }
    }
}